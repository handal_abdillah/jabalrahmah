<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function index(){
        $data["sliders"] = $this->db->get_where("home_slider", array("aktif_slider"=>true))->result();
        $data["general_content"] = $this->db->get("general_content")->result();
        $data["list_cat_galery"] = $this->db->get("galery_category")->result();
        $data["list_galery"] = $this->db->get_where("galery", array("is_active"=>true), 12)->result();
        $data["list_berita"] = $this->db->get("posting", 3)->result();
        $data["list_program"] = $this->db->get_where("program", array("aktif"=>true))->result();
        $this->display_page("home", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/header_template",
            "main_content" => $main_content,
            "footer_template" => "template/footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

}