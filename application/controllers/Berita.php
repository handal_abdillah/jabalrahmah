<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

    function index(){
        $this->l();
    }

    function l($page=null){
        $page = empty($page) ? 1 : $page; 
        $size = NEWS_PERPAGE;
        $this->load->model("data_model");
        $data["recent_articles"] = $this->data_model->get_recent_article();
        $data["list_articles"] = $this->data_model->get_recent_article($page, $size);
        $data["list_category"] = $this->db->get("post_category")->result();
        $data["cur_page"] = $page;
        $this->display_page("berita", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/header_template",
            "main_content" => $main_content,
            "footer_template" => "template/footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function detail($slug){
        $slug_art = explode("-", $slug);
        if(isset($slug_art[0]) && intval($slug_art[0])){
            $this->load->model("data_model");
            $data["recent_articles"] = $this->data_model->get_recent_article();
            $data["list_category"] = $this->db->get("post_category")->result();
            $data["detail_posting"] = $this->data_model->get_detail_berita(intval($slug_art[0]));
            // echo "<pre >"; var_dump($this->db->last_query()); exit;
            $this->display_page("berita_detail", $data);
        }else{
            redirect("berita");
        }
        
    }
}