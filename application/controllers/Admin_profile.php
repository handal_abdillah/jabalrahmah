<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_profile extends CI_Controller {
    function __construct() {
        parent::__construct();
        $usr_ses = $this->session->userdata();
        if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
            redirect("auth");
        }
    }
    function index(){
        $this->profile();
    }

    function profile(){
        $profile_content = $this->db->get_where("general_content",array("initial_page" => INIT_PG_PROF))->result_array();
        $data_profile = array();
        foreach($profile_content as $content){
            $data_profile[$content["key_content"]] = $content["value_content"];
        }
        $data["profile_content"] = $data_profile;
        $this->display_page_admin("admin_profile", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/admin_header_template",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template/admin_footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function display_page_admin($main_content, $my_data = array()){
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function save(){
        $this->load->model("data_model");
        $profile_post = $this->input->post();
        
        $this->db->delete("general_content", array('initial_page'=>INIT_PG_PROF, 'key_content !='=>'IMAGE_PROFILE'));

        $upl_img = $this->data_model->upload_image(DIR_IMG, "IMAGE_PROFILE", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $profile_post["IMAGE_PROFILE"] = $upl_img["msg"]["file_name"];
            $this->db->delete("general_content", array('initial_page'=>INIT_PG_PROF));
        }
        $data_profile = array();
        while ($content_profile = current($profile_post)) {
            $data_profile[] = array(
                "key_content" => key($profile_post),
                "value_content" => $content_profile,
                "initial_page" => INIT_PG_PROF
            );
            next($profile_post);
        }

        $this->db->insert_batch("general_content", $data_profile);
        redirect("admin_profile/profile");        
    }

}