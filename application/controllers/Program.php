<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

    function index(){
        $data["list_program"] = $this->db->get_where("program", array("aktif"=>true))->result();
        $this->display_page("program", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/header_template",
            "main_content" => $main_content,
            "footer_template" => "template/footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

}