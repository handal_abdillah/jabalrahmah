<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    function index(){
        $data["list_product"] = $this->db->get("product")->result();
        $this->display_page("product", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/header_template",
            "main_content" => $main_content,
            "footer_template" => "template/footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

}