<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends CI_Controller {
    function __construct() {
        parent::__construct();
        $usr_ses = $this->session->userdata();
        if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
            redirect("auth");
        }
    }
    
    function index(){
        $data["list_user"] = $this->db->get("user_member")->result();
        $this->display_page_admin("admin_user", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/admin_header_template",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template/admin_footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function form_user(){
        $data = array();
        if($this->uri->segment(3)){ 
            $data["detail_user"] = $this->db->get_where("user_member", array("id"=>intval($this->uri->segment(3))))->row_array();
        }
        $this->display_page_admin("form_user", $data);
    }

    function display_page_admin($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function save_user(){
        
        $user_post = $this->input->post();
        $this->load->model("data_model");
        $upl_img = $this->data_model->upload_image(DIR_IMG, "image_profile", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $user_post["image_profile"] = $upl_img["msg"]["file_name"];
        }
        if($this->input->post("is_change")){
            $user_post["pass"] = password_hash($user_post["pass"], PASSWORD_BCRYPT);
        }else{
            unset($user_post["pass"]);
        }
        $user_post["is_aktif"] = $user_post["is_aktif"] && true;
        unset($user_post["is_change"]);
        if($user_post["id_user"]){
            $id_user = $user_post["id_user"];
            unset($user_post["id_user"]);
            $this->db->update('user_member', $user_post, array("id"=>$id_user));
        }else{
            unset($user_post["id_user"]);
            $this->db->insert('user_member', $user_post);
        }
        redirect("admin_user");
    }

    function del_user(){
        $ret = array(
            "err"=>0,
            "msg"=>null,
        );
        if($this->input->post("id_user")){
            
            $this->db->delete('program', array('id' => $this->input->post("id_user"))); 
            $ret["err"] = false;
            $ret["msg"] = "Delete User success";
            
        }else{
            $ret["err"] = true;
            $ret["msg"] = "Failed to delete User, because no selected User";
        }
        echo json_encode($ret);
    }

}