<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    function index(){
        $this->load->model("data_model");
        $data = $this->data_model->get_general_content();
        $this->display_page("profile", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/header_template",
            "main_content" => $main_content,
            "footer_template" => "template/footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

}