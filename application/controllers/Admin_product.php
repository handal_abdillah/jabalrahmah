<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_product extends CI_Controller {

    function __construct() {
        parent::__construct();
        $usr_ses = $this->session->userdata();
        if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
            redirect("auth");
        }
    }

    function index(){
        $data["list_product"] = $this->db->get("product")->result();
        $this->display_page_admin("admin_product", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/admin_header_template",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template/admin_footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function form_product(){
        $data = array();
        if($this->uri->segment(3)){ 
            $data["detail_product"] = $this->db->get_where("product", array("id"=>intval($this->uri->segment(3))))->row_array();
        }
        $this->display_page_admin("form_product", $data);
    }

    function display_page_admin($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function form_category(){
        $data = array();
        if($this->input->get("id_category")){
            $data["detail_category"] = $this->db->get_where("post_category", array("id"=>$this->input->get("id_category")))->row_array();
        }
        echo $this->load->view("admin/pop_form_category", $data, true);
    }

    function save_product(){
        
        $product_post = $this->input->post();
        $this->load->model("data_model");
        $upl_img = $this->data_model->upload_image(DIR_IMG, "image_product", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $product_post["image_product"] = $upl_img["msg"]["file_name"];
        }
        if($product_post["id_product"]){
            $id_product = $product_post["id_product"];
            unset($product_post["id_product"]);
            $this->db->update('product', $product_post, array("id"=>$id_product));
        }else{
            unset($product_post["id_product"]);
            $this->db->insert('product', $product_post);
        }
        redirect("admin_product");
    }

    function del_product(){
        $ret = array(
            "err"=>0,
            "msg"=>null,
        );
        if($this->input->post("id_prod")){
            $this->db->delete('product', array('id' => $this->input->post("id_prod"))); 
            $ret["err"] = false;
            $ret["msg"] = "Delete Product success";
            
        }else{
            $ret["err"] = true;
            $ret["msg"] = "Failed to delete Product, because no selected Product";
        }
        echo json_encode($ret);
    }

}