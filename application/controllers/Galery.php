<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galery extends CI_Controller {

    function index(){
        $data["list_cat_galery"] = $this->db->get("galery_category")->result();
        $data["list_galery"] = $this->db->get("galery")->result();
        $this->display_page("galery", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/header_template",
            "main_content" => $main_content,
            "footer_template" => "template/footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

}