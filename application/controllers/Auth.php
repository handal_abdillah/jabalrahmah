<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function index(){
        $this->login();
    }

    function login(){
        $this->load->view("login");
    }

    function do_auth(){
        if($this->input->post("email") && $this->input->post("password")){
            $detail_user = $this->db->get_where("user_member", array("email"=>$this->input->post("email")))->row();
            if(!empty($detail_user) && isset($detail_user->pass) && password_verify($this->input->post("password"), $detail_user->pass)){
                $usr_sess = array(
                    "id" => $detail_user->id,
                    "full_name" => $detail_user->full_name,
                    "email" => $detail_user->email,
                    "image_profile" => $detail_user->image_profile
                );
                $this->session->set_userdata($usr_sess);
                $this->db->update("user_member", array("last_login"=>date("Y-m-d H:i:s")), array("id"=>$detail_user->id));
                redirect("admin");
            }else{
                // echo "<pre>"; var_dump(!empty($detail_user), isset($detail_user->pass), password_verify($this->input->post("password"), $detail_user->pass)); exit;
                redirect("auth");
            }
        }else{
            redirect("auth");
            // echo "<pre>"; var_dump("no post",$this->input->post("username"),$this->input->post("password")); exit;
        }
        
    }

    function destroy_sess(){
        $this->session->sess_destroy();
        redirect("auth");
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/admin_header_template",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template/admin_footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function a(){
        $this->display_page_admin("test");
    }

    function display_page_admin($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function form_slider(){
        $data = array();
        if($this->input->get("id_slider")){
            $data["detail_slider"] = $this->db->get_where("home_slider", array("id"=>$this->input->get("id_slider")))->row_array();
        }
        echo $this->load->view("admin/pop_form_slider", $data, true);
    }

    function save_slider(){
        $slider_post = $this->input->post();
        $this->load->model("data_model");
        $upl_img = $this->data_model->upload_image(DIR_IMG, "image_slider", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $slider_post["img_slider"] = $upl_img["msg"]["file_name"];
        }
        $slider_post["aktif_slider"] = strtoupper($slider_post["aktif_slider"]) == "Y";
        
        if($slider_post["id_slider"]){
            $id_slider = $slider_post["id_slider"];
            unset($slider_post["id_slider"]);
            $this->db->update('home_slider', $slider_post, array("id"=>$id_slider));
        }else{
            unset($slider_post["id_slider"]);
            $this->db->insert('home_slider', $slider_post);
        }
        
        redirect("admin");
    }

    function save_welcome_text(){
        if($this->input->post("WELCOME_TEXT")){
            $welcome_text = $this->db->get_where("general_content", array("key_content"=>$this->input->post("WELCOME_TEXT")))->row();
            $data_welcome = array(
                "key_content"=>"WELCOME_TEXT",
                "value_content"=>$this->input->post("WELCOME_TEXT"), 
                "initial_page"=>INIT_PG_HOM
            );
            if(!empty($welcome_text)){
                $this->db->insert("general_content",$data_welcome);
            }else{
                $this->db->update("general_content", $data_welcome, array("key_content"=>"WELCOME_TEXT"));
            }
        }
        redirect("admin");
    }

}