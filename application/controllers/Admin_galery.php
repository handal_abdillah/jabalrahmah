<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_galery extends CI_Controller {
    function __construct() {
        parent::__construct();
        $usr_ses = $this->session->userdata();
        if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
            redirect("auth");
        }
    }
    
    function index(){
        $data["list_image"] = $this->db->get("galery")->result();
        $this->display_page_admin("admin_galery", $data);
    }


    function display_page_admin($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function form_galery(){
        $data = array();
        $data["list_category"] = $this->db->get("galery_category")->result();
        if($this->uri->segment(3)){ 
            $data["detail_galery"] = $this->db->get_where("galery", array("id"=>intval($this->uri->segment(3))))->row_array();
        }
        $this->display_page_admin("form_galery", $data);
    }

    function form_cat_galery(){
        echo $this->load->view("admin/form_cat_galery", null, true);
    }
    function save_cat_galery(){
        $cat_gal = $this->input->post();
        $id_cat_gal = $cat_gal["id_galery"];
        if(isset($cat_gal["id_cat_galery"]) && $cat_gal["id_cat_galery"]){
            $id_cat_gal = $cat_gal["id_cat_galery"];
            unset($cat_gal["id_cat_galery"]);
            $this->db->update('galery_category', $cat_gal, array("id"=>$id_galery));
        }else{
            unset($cat_gal["id_cat_galery"]);
            $this->db->insert('galery_category', $cat_gal);
        }

        redirect("admin_galery/form_galery");
    }
    function save_galery(){
        $galery_post = $this->input->post();
        $this->load->model("data_model");
        $upl_img = $this->data_model->upload_image(DIR_IMG, "img_loc", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $galery_post["img_loc"] = $upl_img["msg"]["file_name"];
        }else{
            $notif= array(
                "msg"=> "failed to upload image, because ".$upl_img["msg"],
                "type"=> "error"
            );
            $this->session->set_flashdata("notif", $notif);
        }
        $galery_post["is_active"] = strtoupper($galery_post["is_active"]) == "ON";
        if($galery_post["id_galery"]){
            $id_galery = $galery_post["id_galery"];
            unset($galery_post["id_galery"]);
            $this->db->update('galery', $galery_post, array("id"=>$id_galery));
        }else{
            unset($galery_post["id_galery"]);
            $this->db->insert('galery', $galery_post);
        }
        redirect("admin_galery");
    }

    function save_welcome_text(){
        if($this->input->post("WELCOME_TEXT")){
            $welcome_text = $this->db->get_where("general_content", array("key_content"=>$this->input->post("WELCOME_TEXT")))->row();
            $data_welcome = array(
                "key_content"=>"WELCOME_TEXT",
                "value_content"=>$this->input->post("WELCOME_TEXT"), 
                "initial_page"=>INIT_PG_HOM
            );
            if(!empty($welcome_text)){
                $this->db->insert("general_content",$data_welcome);
            }else{
                $this->db->update("general_content", $data_welcome, array("key_content"=>"WELCOME_TEXT"));
            }
        }
        redirect("admin");
    }

    function del_galery(){
        $ret = array(
            "err"=>0,
            "msg"=>null,
        );
        if($this->input->post("id_gal")){
            $this->db->delete('galery', array('id' => $this->input->post("id_gal"))); 
            $ret["err"] = false;
            $ret["msg"] = "Delete Photo success";
            
        }else{
            $ret["err"] = true;
            $ret["msg"] = "Failed to delete Photo, because no selected Photo";
        }
        echo json_encode($ret);
    }

}