<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_program extends CI_Controller {
    function __construct() {
        parent::__construct();
        $usr_ses = $this->session->userdata();
        if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
            redirect("auth");
        }
    }
    function index(){
        $this->program();
    }

    function program(){
        // $this->load->view("admin/admin_home");
        $data["list_program"] = $this->db->get("program")->result();
        $this->display_page_admin("admin_program", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/admin_header_template",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template/admin_footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function display_page_admin($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function form_program(){
        $data = array();
        if($this->input->get("id_program")){
            $data["detail_program"] = $this->db->get_where("program", array("id"=>$this->input->get("id_program")))->row_array();
        }
        echo $this->load->view("admin/pop_form_program", $data, true);
    }

    function save_program(){
        $program_post = $this->input->post();
        $this->load->model("data_model");
        $upl_img = $this->data_model->upload_image(DIR_IMG, "image_program", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $program_post["image_program"] = $upl_img["msg"]["file_name"];
        }
        $program_post["aktif"] = strtoupper($program_post["aktif"]) == "Y";
        
        if($program_post["id_program"]){
            $id_slider = $program_post["id_program"];
            unset($program_post["id_program"]);
            $this->db->update('program', $program_post, array("id"=>$id_slider));
        }else{
            unset($program_post["id_program"]);
            $this->db->insert('program', $program_post);
        }
        
        redirect("admin_program");
    }

    function del_program(){
        $ret = array(
            "err"=>0,
            "msg"=>null,
        );
        if($this->input->post("id_prog")){
            
            $this->db->delete('program', array('id' => $this->input->post("id_prog"))); 
            $ret["err"] = false;
            $ret["msg"] = "Delete Program success";
            
        }else{
            $ret["err"] = true;
            $ret["msg"] = "Failed to delete Program, because no selected Program";
        }
        echo json_encode($ret);
    }
}