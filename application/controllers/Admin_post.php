<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_post extends CI_Controller {

    function __construct() {
        parent::__construct();
        $usr_ses = $this->session->userdata();
        if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
            redirect("auth");
        }
    }

    function index(){
        $data["list_article"] = $this->db->get("posting")->result();
        $this->display_page_admin("admin_post", $data);
    }

    function category(){
        
        $data["list_category"] = $this->db->get("post_category")->result();
        $this->display_page_admin("admin_category_post", $data);
    }

    function display_page($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template/admin_header_template",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template/admin_footer_template"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template/template", $data);
    }

    function form_post(){
        $data = array();
        $data["list_category"] = $this->db->get("post_category")->result();
        if($this->uri->segment(3)){ 
            $data["detail_posting"] = $this->db->get_where("posting", array("id"=>intval($this->uri->segment(3))))->row_array();
        }
        $this->display_page_admin("form_posting", $data);
    }

    function display_page_admin($main_content, $my_data = array())
    {
        $data = array(
            "header_template" => "template_admin/template_header",
            "main_content" => "admin/".$main_content,
            "footer_template" => "template_admin/template_footer"
        );
        $data = array_merge($data, $my_data);
        $this->load->view("template_admin/template", $data);
    }

    function form_category(){
        $data = array();
        if($this->input->get("id_category")){
            $data["detail_category"] = $this->db->get_where("post_category", array("id"=>$this->input->get("id_category")))->row_array();
        }
        echo $this->load->view("admin/pop_form_category", $data, true);
    }

    function save_category(){
        $category_post = $this->input->post();
        if($category_post["id_category"]){
            $id_cat = $category_post["id_category"];
            unset($category_post["id_category"]);
            $this->db->update('post_category', $category_post, array("id"=>$id_cat));
        }else{
            unset($category_post["id_category"]);
            $this->db->insert('post_category', $category_post);
        }
        redirect("admin_post/category");
    }

    function save_posting(){
        $usr_ses = $this->session->userdata();
        $posting_post = $this->input->post();
        $posting_post["author"] = $this->session->userdata("id");
        $this->load->model("data_model");
        $upl_img = $this->data_model->upload_image(DIR_IMG, "image_header", 'gif|jpg|png|jpeg');
        if(!$upl_img["error"]){
            $resize_img = $this->data_model->resize_image($upl_img["msg"]["full_path"]);
            $posting_post["image_header"] = $upl_img["msg"]["file_name"];
        }
        if($posting_post["id_posting"]){
            $id_posting = $posting_post["id_posting"];
            unset($posting_post["id_posting"]);
            $this->db->update('posting', $posting_post, array("id"=>$id_posting));
        }else{
            unset($posting_post["id_posting"]);
            $posting_post["author"] = $usr_ses["id"];
            $this->db->insert('posting', $posting_post);
        }
        redirect("admin_post");
    }

    function del_cat(){
        $ret = array(
            "err"=>0,
            "msg"=>null,
        );
        if($this->input->post("id_cat")){
            $artcl = $this->db->get_where("posting", array("id_category"=>$this->input->post("id_cat")))->result();
            if(empty($artcl)){
                $this->db->delete('post_category', array('id' => $this->input->post("id_cat"))); 
                $ret["err"] = false;
                $ret["msg"] = "Delete category success";
            }else{
                $ret["err"] = true;
                $ret["msg"] = "Failed to delete category, because category been used in post";
            }
        }else{
            $ret["err"] = true;
            $ret["msg"] = "Failed to delete category, because no selected category";
        }
        echo json_encode($ret);
    }

    function del_post(){
        $ret = array(
            "err"=>0,
            "msg"=>null,
        );
        if($this->input->post("id_post")){
            
            $this->db->delete('posting', array('id' => $this->input->post("id_post"))); 
            $ret["err"] = false;
            $ret["msg"] = "Delete posting success";
            
        }else{
            $ret["err"] = true;
            $ret["msg"] = "Failed to delete posting, because no selected posting";
        }
        echo json_encode($ret);
    }

}