<?php
class Data_model extends CI_Model {
        public function __construct(){
                $this->load->database();
        }

        function upload_image($upload_path, $file_name, $allowed_types="*"){
                $ret_data = array("error"=>true, "msg"=>"");
                if(isset($_FILES[$file_name])){
                        $config['allowed_types'] = $allowed_types;
                        $config['upload_path']          = $upload_path;
                        $config['max_size']             = 10000;
                        $this->load->library('upload', $config);
                        if ( ! $this->upload->do_upload($file_name)){
                                $ret_data = array("error"=>true, "msg"=>$this->upload->display_errors());
                        }
                        else{
                                $ret_data = array("error"=>false, "msg"=>$this->upload->data());

                        }
                }else{
                        $ret_data = array("error"=>false, "msg"=>"");
                }
                return $ret_data;
        }

        function resize_image($full_path_img, $max_size=650){
                $ret_data = array("error"=>true, "msg"=>"");
                if(file_exists($full_path_img)){
                        $this->load->library('image_lib');
                        $image_data =   $this->upload->data();
                        $configer =  array(
                        'image_library'   => 'gd2',
                        'create_thumb' => FALSE,
                        'source_image'    =>  $full_path_img,
                        'maintain_ratio'  =>  TRUE,
                        'width'           =>  $max_size,
                        'height'          =>  $max_size,
                        );
                        $this->image_lib->clear();
                        $this->image_lib->initialize($configer);
                        if ( ! $this->image_lib->resize()){
                                $ret_data = array("error"=>true, "msg"=>$this->image_lib->display_errors());
                        }else{
                                $ret_data = array("error"=>false, "msg"=>"");
                        }
                }else{
                        $ret_data = array("error"=>true, "msg"=>"file not found !");
                }
                
                return $ret_data;
        }

        function get_general_content(){
                $general_content = $this->db->get("general_content")->result();
                $data["welcome_text"] = "";
                $data["prof_img"] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                $data["prof_text"] = "";
                $data["prof_phone"] = "";
                $data["prof_web"] = "";
                $data["prof_mail"]= "";
                $data["prof_ofchr"] = "";
                $data["prof_addr"] = "";

                foreach($general_content as $gc){
                        if(isset($gc->key_content) && $gc->key_content == "WELCOME_TEXT") $data["welcome_text"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "TITLE_PROFILE") $data["title_profile"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "IMAGE_PROFILE") $data["prof_img"] = base_url()."assets/img/".$gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "DESKRIPSI_PROFILE") $data["prof_text"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "PHONE_PROFILE") $data["prof_phone"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "URL_PROFILE") $data["prof_web"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "EMAIL_PROFILE") $data["prof_mail"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "OFFICE_HOUR") $data["prof_ofchr"] = $gc->value_content;
                        if(isset($gc->key_content) && $gc->key_content == "ADDRESS_PROFILE") $data["prof_addr"] = $gc->value_content;
                }

                return $data;

        }

        function get_recent_article($page=1, $size=5){
                $this->db->from("posting");
                $this->db->limit($size, ($page-1)*$size);
                $this->db->order_by("posting_time", "desc");
                return $this->db->get()->result();
        }

        function get_total_article(){
                return $this->db->get("posting")->num_rows();
        }

        function get_detail_berita($id){
                $this->db->select("posting.*, post_category.name_category, user_member.full_name");
                $this->db->from("posting");
                $this->db->join("post_category", "posting.id_category = post_category.id", "left");
                $this->db->join("user_member", "posting.author = user_member.id", "left");
                $this->db->where(array("posting.id"=>intval($id)));
                return $this->db->get()->row();
        }
}