<div class="blog-page area-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="page-head-blog">
            <div class="single-blog-page">
              <!-- recent start -->
              <div class="left-blog">
                <h4>recent post</h4>
                <div class="recent-post">
                  
                  <?php 
                    if(isset($recent_articles)){
                      foreach($recent_articles as $rec_art){
                        echo '
                        <!-- start single post -->
                        <div class="recent-single-post">
                          <div class="post-img">
                            <a href="#">
                                  <img src="'.base_url().'assets/img/'.$rec_art->image_header.'" alt="'.$rec_art->title.'">
                              </a>
                          </div>
                          <div class="pst-content">
                            <p><a href="'.base_url().'/berita/detail/'.$rec_art->id.'-'.$rec_art->slug.'">'.$rec_art->title.'</a></p>
                          </div>
                        </div>
                        <!-- End single post -->';
                      }
                    }
                  ?>
                </div>
              </div>
              <!-- recent end -->
            </div>
            <div class="single-blog-page">
              <div class="left-blog">
                <h4>categories</h4>
                <ul>
                <?php 
                  if(isset($list_category)){
                    foreach($list_category as $cat){
                      echo '<li><a href="'.base_url().'berita/category/'.$cat->slug.'">'.$cat->name_category.'</a></li>';
                    }
                  }
                ?>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
        <!-- End left sidebar -->
        <!-- Start single blog -->
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <!-- single-blog start -->
              <article class="blog-post-wrapper">
                <div class="post-thumbnail">
                  <img src="<?php echo isset($detail_posting->image_header) ? base_url()."assets/img/".$detail_posting->image_header : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>"alt="" />
                </div>
                <div class="post-information">
                  <h2><?php echo isset($detail_posting->title) ? $detail_posting->title : null;?></h2>
                  <div class="entry-meta">
                    <span class="author-meta"><i class="fa fa-user"></i> <a href="#"><?php echo isset($detail_posting->full_name) ? $detail_posting->full_name : null;?></a></span>
                    <span><i class="fa fa-clock-o"></i> <?php echo isset($detail_posting->posting_time) ? $detail_posting->posting_time : null;?></span>
                    <span class="tag-meta">
												<i class="fa fa-folder-o"></i>
												<a href="#"><?php echo isset($detail_posting->name_category) ? $detail_posting->name_category : null;?></a>
											</span>
                  </div>
                  <div class="entry-content">
                  <p>
                  <?php echo isset($detail_posting->posting_content) ? $detail_posting->posting_content : null;?>
                  </p>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>