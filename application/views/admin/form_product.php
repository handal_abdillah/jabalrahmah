<div id="modal-cat-gal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" id="form_cat_gal">
  </div>
</div>
<!-- Switchery -->
<link href="<?php echo base_url();?>assets_admin/switchery/dist/switchery.min.css" rel="stylesheet">

<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Form Product</h3>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" action="<?php echo base_url();?>admin_product/save_product" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <input type="hidden" name="id_product" value="<?php echo isset($detail_product["id"]) ? $detail_product["id"] : "";?>"/>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Nama Product</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="nama_product" value="<?php echo isset($detail_product["nama_product"]) ? $detail_product["nama_product"] : "";?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="fileupload-new thumbnail" style="height:auto !important;">
                                    <img alt="" src="<?php echo isset($detail_product["image_product"]) ? base_url()."assets/img/".$detail_product["image_product"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                                </div>
                                <input type="file" class="default" name="image_product" accept="image/*"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Deskripsi Product</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control col-md-7 col-xs-12" name="deskripsi_product" rows="5"><?php echo isset($detail_product["deskripsi_product"]) ? $detail_product["deskripsi_product"] : "";?></textarea>
                        </div>
                    </div>
                    <div class="form-actions col-md-12" style="text-align:right;">
                        <a href="<?php echo base_url()."admin_product";?>" class="btn btn-warning" type="button"> <i class="fa fa-ban"></i> Cancel</a>
                        <button class="btn btn-success" type="submit"><i class="fa fa-check-circle-o"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>