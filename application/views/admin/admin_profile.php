<script src="<?php echo base_url() . "assets_admin/" ?>ckeditor/ckeditor.js"></script>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Profile Organisasi</h3>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" action="<?php echo base_url();?>admin_profile/save" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Title Profile</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="TITLE_PROFILE" value="<?php echo isset($profile_content["TITLE_PROFILE"]) ? $profile_content["TITLE_PROFILE"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Deskripsi Profile</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="summary_profile" rows="5" class="form-control col-md-7 col-xs-12" name="DESKRIPSI_PROFILE"><?php echo isset($profile_content["DESKRIPSI_PROFILE"]) ? $profile_content["DESKRIPSI_PROFILE"] : "";?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Image Profile</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="fileupload-new thumbnail" style="height:auto !important;">
                                    <img alt="" src="<?php echo isset($profile_content["IMAGE_PROFILE"]) ? base_url()."assets/img/".$profile_content["IMAGE_PROFILE"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                                </div>
                                <input type="file" class="default" name="IMAGE_PROFILE" accept="image/*"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Office Hour</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="OFFICE_HOUR" value="<?php echo isset($profile_content["OFFICE_HOUR"]) ? $profile_content["OFFICE_HOUR"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">address</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="ADDRESS_PROFILE" value="<?php echo isset($profile_content["ADDRESS_PROFILE"]) ? $profile_content["ADDRESS_PROFILE"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="EMAIL_PROFILE" value="<?php echo isset($profile_content["EMAIL_PROFILE"]) ? $profile_content["EMAIL_PROFILE"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Phone</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12"  name="PHONE_PROFILE" value="<?php echo isset($profile_content["PHONE_PROFILE"]) ? $profile_content["PHONE_PROFILE"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Webiste Url</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12"  name="URL_PROFILE" placeholder="http://www.ponpesjabalrahmah.com" value="<?php echo isset($profile_content["URL_PROFILE"]) ? $profile_content["URL_PROFILE"] : "";?>">
                        </div>
                    </div>
                    <div class="form-actions col-md-12" style="text-align:right;">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check-circle-o"></i> Submit</button>
                        <button class="btn btn-warning" type="button"> <i class="fa fa-ban"></i> Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
        var editor = CKEDITOR.replace('summary_profile', {
            toolbar: [
                ['Styles', 'Format', 'Font', 'FontSize'],
                '/',
                ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-', 'Print'],
                '/',
                ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Table', '-', 'Link', 'Flash', 'Smiley', 'TextColor', 'BGColor', 'Source']
            ],
            height: 300,
            width: 600,
        });
    });
</script>

    

                            