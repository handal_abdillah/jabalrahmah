<?php 
    $ychecked = isset($detail_slider["aktif_slider"]) && $detail_slider["aktif_slider"] ? "checked" : "";
    $nchecked = !(isset($detail_slider["aktif_slider"]) && $detail_slider["aktif_slider"]) ? "checked" : "";
?>
<div class="modal-content">
    <div class="modal-body">
        <div class="x_panel" style="border:none;">
            <div class="x_title">
            <h2>Slider Form<small> Home page</small></h2>
            
            <div class="clearfix"></div>
        </div>
            <div class="x_content">

            <!-- start form for validation -->
            <form id="demo-form" action="<?php echo base_url();?>admin/save_slider" method="post" enctype="multipart/form-data" data-parsley-validate>
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                <input type="hidden" name="id_slider" value="<?php echo isset($detail_slider["id"]) ? $detail_slider["id"] : null;?>" />
                <div class="col-md-12">
                    <label for="fullname">Text Button 1 :</label>
                    <input type="text" id="title_slider" class="form-control" name="title_slider" value="<?php echo isset($detail_slider["title_slider"]) ? $detail_slider["title_slider"] : null;?>"/>
                </div>
                <div class="col-md-12">
                    <label for="message">Text Slider (20 chars min, 100 max) :</label>
                    <textarea id="text_slider" required="required" class="form-control" name="text_slider" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"><?php echo isset($detail_slider["text_slider"]) ? $detail_slider["text_slider"] : null;?></textarea>
                    <p></p>
                </div>
                <div class="col-md-6">
                    <label for="fullname">Text Button 1 :</label>
                    <input type="text" id="text_btn1" class="form-control" name="text_btn1" value="<?php echo isset($detail_slider["text_btn1"]) ? $detail_slider["text_btn1"] : null;?>"/>
                </div>
                <div class="col-md-6">
                    <label for="fullname">Link Button 1 :</label>
                    <input type="text" id="link_btn1" class="form-control" name="link_btn1" value="<?php echo isset($detail_slider["link_btn1"]) ? $detail_slider["link_btn1"] : null;?>"/>
                </div>
                <div class="col-md-6">
                    <label for="fullname">Text Button 2 :</label>
                    <input type="text" id="text_btn2" class="form-control" name="text_btn2" value="<?php echo isset($detail_slider["text_btn2"]) ? $detail_slider["text_btn2"] : null;?>"/>
                </div>
                <div class="col-md-6">
                    <label for="fullname">Link Button 2 :</label>
                    <input type="text" id="link_btn2" class="form-control" name="link_btn2" value="<?php echo isset($detail_slider["link_btn2"]) ? $detail_slider["link_btn2"] : null;?>"/>
                </div>
                
                <div class="col-md-12">
                    <label for="fullname">Image Slider :</label>
                    <div data-provides="fileupload" class="fileupload fileupload-new">
                        <div class="fileupload-new thumbnail" style="height:auto !important;">
                            <img alt="" src="<?php echo isset($detail_slider["img_slider"]) ? base_url()."assets/img/".$detail_slider["img_slider"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                        </div>
                        <input type="file" class="default" name="image_slider" accept="image/*"></span>
                    </div>
                </div>
                <div class="col-md-12">
                <br/>
                <label>Active *:</label>
                <p>
                Yes: <input type="radio" class="flat" name="aktif_slider" id="activeY" value="Y" <?php echo $ychecked;?> required /> 
                No: <input type="radio" class="flat" name="aktif_slider" id="activeY" value="N" <?php echo $nchecked;?>/>
                </p>
                </div>
                <p>
                <br/>
                <div class="col-md-12" style="text-align:right;">
                <br/>
                    <button class="btn btn-primary" type="submit"><i class="fa fa-check-circle-o"></i> Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close"> <i class="fa fa-ban"></i>  Cancel</button>
                </div>
            </form>
            <!-- end form for validations -->

            </div>
        </div>
    </div>
</div>