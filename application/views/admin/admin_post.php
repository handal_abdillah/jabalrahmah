
<div id="modal-slider" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" id="form-slider">
    
  </div>
</div>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>List Category</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="<?php echo base_url();?>admin_post/form_post" class="btn btn-block btn-round btn-success"><i class="fa fa-plus"></i> Add Posting</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th class="hidden-phone">Author</th>
                        <th class="hidden-phone">Category</th>
                        <th class="hidden-phone">Tags</th>
                        <th style="width:8px;">Posting Date</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(isset($list_article)){
                                foreach($list_article as $article){
                                    // $check = $$article->aktif_slider ? 'checked' : '';
                                    echo '<tr class="odd gradeX">
                                    <td>'.$article->title.'</td>
                                    <td class="hidden-phone">'.$article->author.'</td>
                                    <td class="hidden-phone">'.$article->id_category.'</td>
                                    <td class="hidden-phone">'.$article->tags.'</td>
                                    <td class="hidden-phone">'.$article->posting_time.'</td>
                                    <td class="hidden-phone">
                                        <a class="btn btn-warning" href="'.base_url().'admin_post/form_post/'.$article->id.'"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger" onclick="del_post(\''.$article->id.'\')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<script>
    function pop_form_category(){
        $("#form-slider").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_post/form_category",
            method: "GET",
            dataType: "html",
            success : function(fslider){
                $("#form-slider").append(fslider);
            }
        }).done(function(){
            $("#modal-slider").modal("show")
        })
    }

    function edit_category(id_category){
        $("#form-slider").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_post/form_category",
            method: "GET",
            data : {id_category:id_category},
            dataType: "html",
            success : function(fslider){
                $("#form-slider").append(fslider);
            }
        }).done(function(){
            $("#modal-slider").modal("show")
        })
    }

    function del_post(id_post){
        if(confirm("anda yakin akan menghapus artikel ini ?")){
            var msg = "";
            $.ajax({
                url: "<?php echo base_url();?>admin_post/del_post",
                method: "POST",
                data : {id_post:id_post, <?php echo $this->security->get_csrf_token_name();?>:"<?php echo $this->security->get_csrf_hash();?>"},
                dataType: "json",
                success : function(res){
                    msg = res.msg;
                }
            }).done(function(){
                alert(msg);
                location.reload();
            })
        }
    }
</script>

<script src="<?php echo base_url();?>assets_admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets_admin/jquery.tagsinput/src/jquery.tagsinput.js"></script>



    

                            