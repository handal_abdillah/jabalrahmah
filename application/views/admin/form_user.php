<!-- Switchery -->
<link href="<?php echo base_url();?>assets_admin/switchery/dist/switchery.min.css" rel="stylesheet">

<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Form User</h3>
        </div>

        <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
            </span>
            </div>
        </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" id="form-user" action="<?php echo base_url();?>admin_user/save_user" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <input type="hidden" name="id_user" value="<?php echo isset($detail_user["id"]) ? $detail_user["id"] : "";?>"/>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Full Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="full_name" value="<?php echo isset($detail_user["full_name"]) ? $detail_user["full_name"] : "";?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="email" class="form-control col-md-7 col-xs-12" name="email" value="<?php echo isset($detail_user["email"]) ? $detail_user["email"] : "";?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Header Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="fileupload-new thumbnail" style="height:auto !important;">
                                    <img alt="" src="<?php echo isset($detail_user["image_profile"]) ? base_url()."assets/img/".$detail_user["image_profile"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                                </div>
                                <input type="file" class="default" name="image_profile" accept="image/*"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12" for="fullname">Password:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="pass" onchange="change_pass()" class="form-control" name="pass" value="<?php echo isset($detail_user["pass"]) ? $detail_user["pass"] : null;?>"/>
                            <input type="hidden" name="is_change" id="is_change">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12" for="fullname">Verify Password:</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="pass1" class="form-control" value="<?php echo isset($detail_user["pass"]) ? $detail_user["pass"] : null;?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="checkbox" class="js-switch" name="is_aktif" <?php echo isset($detail_user["is_aktif"]) &&  $detail_user["is_aktif"] == '1' ? 'checked' : '' ;?>/>
                        </div>
                    </div>
                    <div class="form-actions col-md-12" style="text-align:right;">
                        <a href="<?php echo base_url()."admin_user";?>" class="btn btn-warning" type="button"> <i class="fa fa-ban"></i> Cancel</button>
                        <button class="btn btn-success" type="button" onclick="validate_form()"><i class="fa fa-check-circle-o"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Switchery -->
<script src="<?php echo base_url();?>assets_admin/switchery/dist/switchery.min.js"></script>
<script>
    function validate_form(){
        if($("#pass").val() == $("#pass1").val()){
            $("#form-user").submit();
        }else{
            alert("password is not match");
        }
    }

    function change_pass(){
        $("#is_change").val("1");
    }
</script>