<div id="modal-cat-gal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" id="form_cat_gal">
  </div>
</div>
<!-- Switchery -->
<link href="<?php echo base_url();?>assets_admin/switchery/dist/switchery.min.css" rel="stylesheet">

<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Form Galery</h3>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" id="form-user" action="<?php echo base_url();?>admin_galery/save_galery" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <input type="hidden" name="id_galery" value="<?php echo isset($detail_galery["id"]) ? $detail_galery["id"] : "";?>"/>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="fileupload-new thumbnail" style="height:auto !important;">
                                    <img alt="" src="<?php echo isset($detail_galery["img_loc"]) ? base_url()."assets/img/".$detail_galery["img_loc"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                                </div>
                                <input type="file" class="default" name="img_loc" accept="image/*"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Alternatif Text</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="alt_text" value="<?php echo isset($detail_galery["alt_text"]) ? $detail_galery["alt_text"] : "";?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category</label>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <select class="form-control" name="id_cat" value="<?php echo isset($detail_posting["id_cat"]) ? $detail_posting["id_cat"] : "";?>">
                            <option value="">-Choose Category-</option>
                          <?php 
                            if(isset($list_category)){
                                foreach($list_category as $acat){
                                    $selected = isset($detail_posting["id_cat"]) & $detail_posting["id_cat"] == $acat->id ? "selected" : "";
                                    echo "<option value='".$acat->id."' ".$selected.">".$acat->cat_name."</option>";
                                }
                            }
                          ?>
                          </select>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <label><a href="#" onclick="popup_cat()">+ Add category</a></label>
                            
                        </div>
                      </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="checkbox" class="js-switch" name="is_active" <?php echo isset($detail_galery["is_active"]) &&  $detail_galery["is_active"]? 'checked' : '' ;?>/>
                        </div>
                    </div>
                    <div class="form-actions col-md-12" style="text-align:right;">
                        <a href="<?php echo base_url()."admin_galery";?>" class="btn btn-warning" type="button"> <i class="fa fa-ban"></i> Cancel</a>
                        <button class="btn btn-success" type="submit"><i class="fa fa-check-circle-o"></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Switchery -->

<script>
    function popup_cat(){
        $("#form_cat_gal").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_galery/form_cat_galery",
            method: "GET",
            dataType: "html",
            success : function(test){
                console.log(test);
                $("#form_cat_gal").append(test);
            }
        }).done(function(){
            $("#modal-cat-gal").modal("show")
        })
    }
    
</script>
<script src="<?php echo base_url();?>assets_admin/switchery/dist/switchery.min.js"></script>