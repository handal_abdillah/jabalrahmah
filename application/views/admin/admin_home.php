<div id="modal-slider" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" id="form-slider">
    
  </div>
</div>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Home admin</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <button type="button" class="btn btn-block btn-round btn-success" onclick="pop_form()"><i class="fa fa-plus"></i> Add Text Slider</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Title Slider</th>
                        <th>Text Slider</th>
                        <th class="hidden-phone">Button 1</th>
                        <th class="hidden-phone">Button 2</th>
                        <th class="hidden-phone">Updated</th>
                        <th style="width:8px;">Aktif</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(isset($list_slider)){
                                foreach($list_slider as $aslider){
                                    $check = $aslider->aktif_slider ? 'checked' : '';
                                    echo '<tr class="odd gradeX">
                                    <td>'.$aslider->title_slider.'</td>
                                    <td>'.$aslider->text_slider.'</td>
                                    <td class="hidden-phone">Button Text : <b>'.$aslider->text_btn1.'<b><br>url : <a href="'.$aslider->link_btn1.'">'.$aslider->link_btn1.'</a></td>
                                    <td class="hidden-phone">Button Text : <b>'.$aslider->text_btn2.'<b><br>url : <a href="'.$aslider->link_btn2.'">'.$aslider->link_btn2.'</a></</td>
                                    <td class="center hidden-phone">'.$aslider->updated_time.'</td>
                                    <td><input type="checkbox" class="checkboxes" '.$check.'/></td>
                                    <td class="hidden-phone"><button class="btn btn-warning" onclick="edit_slider(\''.$aslider->id.'\')"><i class="fa fa-pencil"></i></button></td>
                                </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>
            <div class="x_content">
                <form class="form-horizontal form-label-left" action="<?php echo base_url();?>admin/save_welcome_text" method="post">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                  <div class="form-group">
                    <label class="control-label col-md-3" for="first-name">Welcome Text Postal <span class="required">*</span>
                    </label>
                    <div class="col-md-7">
                      <input type="text" id="WELCOME_TEXT" name="WELCOME_TEXT" required="required" value="<?php echo isset($welcome_text->key_content) && !empty($welcome_text) ? $welcome_text->value_content : "";?>" class="form-control col-md-7 col-xs-12">
                    </div>
                    <div class="col-md-2">
                      <button class="btn btn-success"><i class="fa fa-check"></i> UPDATE</button>
                    </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function pop_form(){
        $("#form-slider").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin/form_slider",
            method: "GET",
            dataType: "html",
            success : function(fslider){
                $("#form-slider").append(fslider);
            }
        }).done(function(){
            $("#modal-slider").modal("show")
        })
    }

    function edit_slider(id_slider){
        $("#form-slider").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin/form_slider",
            method: "GET",
            data : {id_slider:id_slider},
            dataType: "html",
            success : function(fslider){
                $("#form-slider").append(fslider);
            }
        }).done(function(){
            $("#modal-slider").modal("show")
        })
    }
</script>

<script src="<?php echo base_url();?>assets_admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/vfs_fonts.js"></script>



    

                            