<?php 
    $ychecked = isset($detail_program["aktif"]) && $detail_program["aktif"] ? "checked" : "";
    $nchecked = !(isset($detail_program["aktif"]) && $detail_program["aktif"]) ? "checked" : "";
?>
<div class="modal-content">
    <div class="modal-body">
        <div class="x_panel" style="border:none;">
            <div class="x_title">
            <h2>Slider Form<small> Home page</small></h2>
            
            <div class="clearfix"></div>
        </div>
            <div class="x_content">

            <!-- start form for validation -->
            <form id="demo-form" action="<?php echo base_url();?>admin_program/save_program" method="post" enctype="multipart/form-data" data-parsley-validate>
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                <input type="hidden" name="id_program" value="<?php echo isset($detail_program["id"]) ? $detail_program["id"] : null;?>" />
                <div class="col-md-12">
                    <label for="fullname">Nama Program :</label>
                    <input type="text" id="title_program" class="form-control" name="title_program" value="<?php echo isset($detail_program["title_program"]) ? $detail_program["title_program"] : null;?>"/>
                </div>
                <div class="col-md-12">
                    <label for="fullname">Image Slider :</label>
                    <div data-provides="fileupload" class="fileupload fileupload-new">
                        <div class="fileupload-new thumbnail" style="height:auto !important;">
                            <img alt="" src="<?php echo isset($detail_program["image_program"]) ? base_url()."assets/img/".$detail_program["image_program"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                        </div>
                        <input type="file" class="default" name="image_program" accept="image/*"></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <label for="message">Deksripsi Program :</label>
                    <textarea id="desc_program" required="required" class="form-control" name="desc_program" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100"><?php echo isset($detail_program["desc_program"]) ? $detail_program["desc_program"] : null;?></textarea>
                    <p></p>
                </div>
                <div class="col-md-12">
                <br/>
                <label>Active *:</label>
                <p>
                Yes: <input type="radio" class="flat" name="aktif" id="activeY" value="Y" <?php echo $ychecked;?> required /> 
                No: <input type="radio" class="flat" name="aktif" id="activeY" value="N" <?php echo $nchecked;?>/>
                </p>
                </div>
                <p>
                <br/>
                <div class="col-md-12" style="text-align:right;">
                <br/>
                    <button class="btn btn-primary" type="submit"><i class="fa fa-check-circle-o"></i> Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close"> <i class="fa fa-ban"></i>  Cancel</button>
                </div>
            </form>
            <!-- end form for validations -->

            </div>
        </div>
    </div>
</div>