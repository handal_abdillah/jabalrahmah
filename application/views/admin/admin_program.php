<div id="modal-program" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" id="form-program">
    
  </div>
</div>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Manage Program</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <button type="button" class="btn btn-block btn-round btn-success" onclick="pop_form()"><i class="fa fa-plus"></i> New Program</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Program Name</th>
                        <th>Program Description</th>
                        <th class="hidden-phone">Last Update</th>
                        <th style="width:8px;">Aktif</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(isset($list_program)){
                                foreach($list_program as $aprog){
                                    $check = $aprog->aktif ? 'checked' : '';
                                    echo '<tr class="odd gradeX">
                                    <td>'.$aprog->title_program.'</td>
                                    <td>'.$aprog->desc_program.'</td>
                                    <td class="center hidden-phone">'.$aprog->updated_time.'</td>
                                    <td><input type="checkbox" class="checkboxes" '.$check.'/></td>
                                    <td class="hidden-phone">
                                        <button class="btn btn-warning" onclick="edit_program(\''.$aprog->id.'\')"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger" onclick="del_program(\''.$aprog->id.'\')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<script>
    function pop_form(){
        $("#form-program").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_program/form_program",
            method: "GET",
            dataType: "html",
            success : function(fslider){
                $("#form-program").append(fslider);
            }
        }).done(function(){
            $("#modal-program").modal("show")
        })
    }

    function edit_program(id_prog){
        $("#form-program").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_program/form_program",
            method: "GET",
            data : {id_program:id_prog},
            dataType: "html",
            success : function(fslider){
                $("#form-program").append(fslider);
            }
        }).done(function(){
            $("#modal-program").modal("show")
        })
    }

    function del_program(id_prog){
        if(confirm("anda yakin akan menghapus program ini ?")){
            var msg = "";
            $.ajax({
                url: "<?php echo base_url();?>admin_program/del_program",
                method: "POST",
                data : {id_prog:id_prog, <?php echo $this->security->get_csrf_token_name();?>:"<?php echo $this->security->get_csrf_hash();?>"},
                dataType: "json",
                success : function(res){
                    msg = res.msg;
                }
            }).done(function(){
                alert(msg);
                location.reload();
            })
        }
        
    }
</script>

<script src="<?php echo base_url();?>assets_admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/vfs_fonts.js"></script>



    

                            