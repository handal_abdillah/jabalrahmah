<!-- BEGIN PAGE -->  
<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title">
                     Slider Text Home
                   </h3>
                   <ul class="breadcrumb">
                       <li>
                           <a href="#">Home</a>
                           <span class="divider">/</span>
                       </li>
                       <li class="active">
                           Slider Text
                       </li>
                       <li class="pull-right search-wrap">
                            <button class="btn btn-block btn-success btn-mtop">Add Slider</button>
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN ADVANCED TABLE widget-->
            <!-- Large modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>

            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <form action="#" class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label">Basic Toggle Button</label>
                                    <div class="controls">
                                        <div id="normal-toggle-button">
                                            <input type="checkbox" checked="checked">
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">WYSIWYG Editor</label>
                                    <div class="controls">
                                        <textarea class="span12 wysihtmleditor5" rows="5"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <table class="table table-striped table-bordered" id="sample_1">
                    <thead>
                    <tr>
                        <th>Text Slider</th>
                        <th class="hidden-phone">Button 1</th>
                        <th class="hidden-phone">Button 2</th>
                        <th class="hidden-phone">Updated</th>
                        <th style="width:8px;">Aktif</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="odd gradeX">
                        <td>Helping Business Security & Peace of Mind for Your Family</td>
                        <td class="hidden-phone">Button Text : <b>See Services<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>
                        <td class="hidden-phone">Button Text : <b>Learn More<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></</td>
                        <td class="center hidden-phone">02.03.2013</td>
                        <td><input type="checkbox" class="checkboxes" value="1" /></td>
                        <td class="hidden-phone"><button class="btn btn-warning"><i class="icon-pencil icon-white"></i></button></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Helping Business Security & Peace of Mind for Your Family</td>
                        <td class="hidden-phone">Button Text : <b>See Services<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>
                        <td class="hidden-phone">Button Text : <b>Learn More<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></</td>
                        <td class="center hidden-phone">02.03.2013</td>
                        <td><input type="checkbox" class="checkboxes" value="1" /></td>
                        <td class="hidden-phone"><button class="btn btn-warning"><i class="icon-pencil icon-white"></i></button></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Helping Business Security & Peace of Mind for Your Family</td>
                        <td class="hidden-phone">Button Text : <b>See Services<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>
                        <td class="hidden-phone">Button Text : <b>Learn More<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></</td>
                        <td class="center hidden-phone">02.03.2013</td>
                        <td><input type="checkbox" class="checkboxes" value="1" /></td>
                        <td class="hidden-phone"><button class="btn btn-warning"><i class="icon-pencil icon-white"></i></button></td>
                    </tr>
                    </tbody>
                </table>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
            <form class="form-horizontal" method="get" action="#">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label" >Welcome Text</label>
                            <div class="controls controls-row">
                                <input type="text" class="input-block-level" placeholder="Text input"  name="">
                            </div>
                        </div>
                    </div>
                </div>  
            </form>
            <!-- END ADVANCED TABLE widget-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  

    <script type="text/javascript" src="<?php echo base_url();?>admin_assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>admin_assets/data-tables/DT_bootstrap.js"></script>
    <link href="<?php echo base_url();?>admin_assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>admin_assets/js/dynamic-table.js"></script>
    <script src="<?php echo base_url();?>admin_assets/js/form-component.js"></script>
