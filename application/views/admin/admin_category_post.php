
<div id="modal-slider" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" id="form-slider">
    
  </div>
</div>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>List Category</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <button type="button" class="btn btn-block btn-round btn-success" onclick="pop_form_category()"><i class="fa fa-plus"></i> Add Category</button>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nama Kategori</th>
                        <th class="hidden-phone">Deskripsi</th>
                        <th class="hidden-phone">Slug</th>
                        <th class="hidden-phone">Keyword</th>
                        <th style="width:8px;">Updated</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(isset($list_category)){
                                foreach($list_category as $acategory){
                                    // $check = $$acategory->aktif_slider ? 'checked' : '';
                                    echo '<tr class="odd gradeX">
                                    <td>'.$acategory->name_category.'</td>
                                    <td class="hidden-phone">'.$acategory->desc_category.'</td>
                                    <td class="hidden-phone">'.$acategory->slug.'</td>
                                    <td class="hidden-phone">'.$acategory->keyword.'</td>
                                    <td class="hidden-phone">'.$acategory->updated_time.'</td>
                                    <td class="hidden-phone">
                                        <button class="btn btn-warning" onclick="edit_category(\''.$acategory->id.'\')"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger" onclick="del_cat(\''.$acategory->id.'\')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<script>
    function pop_form_category(){
        $("#form-slider").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_post/form_category",
            method: "GET",
            dataType: "html",
            success : function(fslider){
                $("#form-slider").append(fslider);
            }
        }).done(function(){
            $("#modal-slider").modal("show")
        })
    }

    function edit_category(id_category){
        $("#form-slider").empty();
        $.ajax({
            url: "<?php echo base_url();?>admin_post/form_category",
            method: "GET",
            data : {id_category:id_category},
            dataType: "html",
            success : function(fslider){
                $("#form-slider").append(fslider);
            }
        }).done(function(){
            $("#modal-slider").modal("show")
        })
    }

    function del_cat(id_cat){
        var msg = "";
        $.ajax({
            url: "<?php echo base_url();?>admin_post/del_cat",
            method: "POST",
            data : {id_cat:id_cat, <?php echo $this->security->get_csrf_token_name();?>:"<?php echo $this->security->get_csrf_hash();?>"},
            dataType: "json",
            success : function(res){
                msg = res.msg;
            }
        }).done(function(){
            alert(msg);
            location.reload();
        })
    }
</script>

<script src="<?php echo base_url();?>assets_admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets_admin/jquery.tagsinput/src/jquery.tagsinput.js"></script>



    

                            