
<div id="modal-slider" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" id="form-slider">
    
  </div>
</div>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Manage Product</h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="<?php echo base_url();?>admin_product/form_product" class="btn btn-block btn-round btn-success"><i class="fa fa-plus"></i> Add product</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>product</th>
                        <th style="width:8px;">Created Time</th>
                        <th class="hidden-phone">Updated Time</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(isset($list_product)){
                                foreach($list_product as $product){
                                    echo '<tr class="odd gradeX">
                                    <td class="hidden-phone">'.$product->nama_product.'</td>
                                    <td><img src="'.base_url().'assets/img/'.$product->image_product.'" alt="'.$product->nama_product.'" height="42" width="42"></td>
                                    <td class="hidden-phone">'.$product->created_time.'</td>
                                    <td class="hidden-phone">'.$product->updated_time.'</td>
                                    <td class="hidden-phone">
                                        <a class="btn btn-warning" href="'.base_url().'admin_product/form_product/'.$product->id.'"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger" onclick="del_product(\''.$product->id.'\')"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>assets_admin/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url();?>assets_admin/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/pdfmake/build/vfs_fonts.js"></script>
<script>
    function del_product(id_prod){
        if(confirm("anda yakin akan menghapus produk ini ?")){
            var msg = "";
            $.ajax({
                url: "<?php echo base_url();?>admin_product/del_product",
                method: "POST",
                data : {id_prod:id_prod, <?php echo $this->security->get_csrf_token_name();?>:"<?php echo $this->security->get_csrf_hash();?>"},
                dataType: "json",
                success : function(res){
                    msg = res.msg;
                }
            }).done(function(){
                alert(msg);
                location.reload();
            })
        }
    }
</script>

    

                            