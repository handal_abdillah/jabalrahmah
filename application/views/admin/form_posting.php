<script src="<?php echo base_url() . "assets_admin/" ?>ckeditor/ckeditor.js"></script>
<div class="right_col" role="main" style="min-height: 1705px;">
    <div class="page-title">
        <div class="title_left">
        <h3>Form Elements</h3>
        </div>

        <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
            </span>
            </div>
        </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <br />
                <form class="form-horizontal form-label-left" action="<?php echo base_url();?>admin_post/save_posting" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                    <input type="hidden" name="id_posting" value="<?php echo isset($detail_posting["id"]) ? $detail_posting["id"] : "";?>"/>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Title Posting</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="title" value="<?php echo isset($detail_posting["title"]) ? $detail_posting["title"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Slug</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control col-md-7 col-xs-12" name="slug" value="<?php echo isset($detail_posting["title"]) ? $detail_posting["title"] : "";?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="id_category" value="<?php echo isset($detail_posting["id_category"]) ? $detail_posting["id_category"] : "";?>">
                            <option value="">-Choose Category-</option>
                          <?php 
                            if(isset($list_category)){
                                foreach($list_category as $acat){
                                    $selected = isset($detail_posting["id_category"]) & $detail_posting["id_category"] == $acat->id ? "selected" : "";
                                    echo "<option value='".$acat->id."' ".$selected.">".$acat->name_category."</option>";
                                }
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Header Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                <div class="fileupload-new thumbnail" style="height:auto !important;">
                                    <img alt="" src="<?php echo isset($detail_posting["image_header"]) ? base_url()."assets/img/".$detail_posting["image_header"] : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                                </div>
                                <input type="file" class="default" name="image_header" accept="image/*"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Posting headline (100 Huruf)</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <textarea class="col-md-8" rows="5" name="posting_headline"><?php echo isset($detail_posting["posting_headline"]) ? $detail_posting["posting_headline"] : "";?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12">Posting Content</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea name="posting_content" id="summary_article" rows="5"><?php echo isset($detail_posting["posting_content"]) ? $detail_posting["posting_content"] : "";?></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label control-label col-md-3 col-sm-3 col-xs-12" for="fullname">Tags (*sparate with comma):</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="tags" class="form-control" name="tags" value="<?php echo isset($detail_posting["tags"]) ? $detail_posting["tags"] : null;?>"/>
                        </div>
                    </div>
                    <div class="form-actions col-md-12" style="text-align:right;">
                        <button class="btn btn-success" type="submit"><i class="fa fa-check-circle-o"></i> Submit</button>
                        <a href="<?php echo base_url();?>admin_post" class="btn btn-warning" type="button"> <i class="fa fa-ban"></i> Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- bootstrap-wysiwyg -->
<script src="<?php echo base_url();?>assets_admin/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo base_url();?>assets_admin/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="<?php echo base_url();?>assets_admin/google-code-prettify/src/prettify.js"></script>

<script src="<?php echo base_url();?>assets_admin/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script>
    $('#tags').tagsInput();
    $(document).ready(function () {
        var editor = CKEDITOR.replace('summary_article', {
            toolbar: [
                ['Styles', 'Format', 'Font', 'FontSize'],
                '/',
                ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'Find', 'Replace', '-', 'Outdent', 'Indent', '-', 'Print'],
                '/',
                ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Image', 'Table', '-', 'Link', 'Flash', 'Smiley', 'TextColor', 'BGColor', 'Source']
            ],
            height: 300,
            width: 600,
            filebrowserBrowseUrl: '<?php echo base_url();?>assets_admin/kcfinder/browse.php?type=files',
            filebrowserImageBrowseUrl: '<?php echo base_url();?>assets_admin/kcfinder/browse.php?type=images',
            // filebrowserImageBrowseUrl: '<?php echo base_url();?>asset/kcfinder/browse.php',
            filebrowserFlashBrowseUrl: '<?php echo base_url();?>assets_admin/kcfinder/browse.php?type=flash',
            filebrowserUploadUrl: '<?php echo base_url();?>assets_admin/kcfinder/upload.php?type=files',
            filebrowserImageUploadUrl: '<?php echo base_url();?>assets_admin/kcfinder/upload.php?type=images',
            filebrowserFlashUploadUrl: '<?php echo base_url();?>assets_admin/kcfinder/upload.php?type=flash'
        });
    });
</script>