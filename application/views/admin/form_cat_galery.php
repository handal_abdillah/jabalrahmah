
<div class="modal-content">
    <div class="modal-body">
        <div class="x_panel" style="border:none;">
            <div class="x_title">
            <h2>Galery Kategori <small>form category</small></h2>
            
            <div class="clearfix"></div>
        </div>
            <div class="x_content">

            <!-- start form for validation -->
            <form id="demo-form" action="<?php echo base_url();?>admin_galery/save_cat_galery" method="post">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                <input type="hidden" name="id_cat_galery" value="<?php echo isset($detail_category["id"]) ? $detail_category["id"] : null;?>" />
                <div class="col-md-12">
                    <label for="fullname">Nama Ketegori Galery:</label>
                    <input type="text" id="name_category" class="form-control" name="cat_name" value="<?php echo isset($detail_category["cat_name"]) ? $detail_category["cat_name"] : null;?>"/>
                </div>
                <div class="col-md-12" style="text-align:right;">
                <br/>
                    <button class="btn btn-primary" type="submit"><i class="fa fa-check-circle-o"></i> Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close"> <i class="fa fa-ban"></i>  Cancel</button>
                </div>
            </form>
            <!-- end form for validations -->

            </div>
        </div>
    </div>
</div>