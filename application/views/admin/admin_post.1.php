<!-- BEGIN PAGE -->  
<div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title">
                     Post
                   </h3>
                   <ul class="breadcrumb">
                       <li>
                           <a href="#">Content</a>
                           <span class="divider">/</span>
                       </li>
                       <li class="active">
                           post
                       </li>
                       <li class="pull-right search-wrap">
                            <button class="btn btn-block btn-success btn-mtop">New Post</button>
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                <!-- BEGIN EXAMPLE TABLE widget-->
                <table class="table table-striped table-bordered" id="sample_1">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th class="hidden-phone">Author</th>
                        <th class="hidden-phone">Categories</th>
                        <th class="hidden-phone">Tags</th>
                        <th style="width:8px;">Publish Date</th>
                        <th class="hidden-phone"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="odd gradeX">
                        <td>Helping Business Security & Peace of Mind for Your Family</td>
                        <td class="hidden-phone">Button Text : <b>See Services<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>
                        <td class="hidden-phone">Button Text : <b>Learn More<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></</td>
                        <td class="center hidden-phone">02.03.2013</td>
                        <td><input type="checkbox" class="checkboxes" value="1" /></td>
                        <td class="hidden-phone"><button class="btn btn-warning"><i class="icon-pencil icon-white"></i></button></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Helping Business Security & Peace of Mind for Your Family</td>
                        <td class="hidden-phone">Button Text : <b>See Services<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>
                        <td class="hidden-phone">Button Text : <b>Learn More<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></</td>
                        <td class="center hidden-phone">02.03.2013</td>
                        <td><input type="checkbox" class="checkboxes" value="1" /></td>
                        <td class="hidden-phone"><button class="btn btn-warning"><i class="icon-pencil icon-white"></i></button></td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>Helping Business Security & Peace of Mind for Your Family</td>
                        <td class="hidden-phone">Button Text : <b>See Services<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></td>
                        <td class="hidden-phone">Button Text : <b>Learn More<b><br>url : <a href="mailto:jhone-doe@gmail.com">jhone-doe@gmail.com</a></</td>
                        <td class="center hidden-phone">02.03.2013</td>
                        <td><input type="checkbox" class="checkboxes" value="1" /></td>
                        <td class="hidden-phone"><button class="btn btn-warning"><i class="icon-pencil icon-white"></i></button></td>
                    </tr>
                    </tbody>
                </table>
                <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
           
            <!-- END ADVANCED TABLE widget-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  

    <script type="text/javascript" src="<?php echo base_url();?>admin_assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>admin_assets/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo base_url();?>admin_assets/js/dynamic-table.js"></script>