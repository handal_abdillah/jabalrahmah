
<?php 
    $ychecked = isset($detail_category["aktif_slider"]) && $detail_category["aktif_slider"] ? "checked" : "";
    $nchecked = !(isset($detail_category["aktif_slider"]) && $detail_category["aktif_slider"]) ? "checked" : "";
?>
<div class="modal-content">
    <div class="modal-body">
        <div class="x_panel" style="border:none;">
            <div class="x_title">
            <h2>Kategori Form <small>posting category</small></h2>
            
            <div class="clearfix"></div>
        </div>
            <div class="x_content">

            <!-- start form for validation -->
            <form id="demo-form" action="<?php echo base_url();?>admin_post/save_category" method="post">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
                <input type="hidden" name="id_category" value="<?php echo isset($detail_category["id"]) ? $detail_category["id"] : null;?>" />
                <div class="col-md-12">
                    <label for="fullname">Nama Ketegori :</label>
                    <input type="text" id="name_category" class="form-control" name="name_category" value="<?php echo isset($detail_category["name_category"]) ? $detail_category["name_category"] : null;?>"/>
                </div>
                <div class="col-md-12">
                    <label for="message">Deskripsi (100 char max) :</label>
                    <textarea id="desc_category" required="required" class="form-control" name="desc_category" data-parsley-trigger="keyup" data-parsley-maxlength="100"><?php echo isset($detail_category["desc_category"]) ? $detail_category["desc_category"] : null;?></textarea>
                    <p></p>
                </div>
                <div class="col-md-12">
                    <label for="fullname">Slug :</label>
                    <input type="text" id="slug" class="form-control" name="slug" value="<?php echo isset($detail_category["slug"]) ? $detail_category["slug"] : null;?>"/>
                </div>
                <div class="col-md-12">
                    <label for="fullname">Keyword (*sparate with comma):</label>
                    <input type="text" id="keyword" class="form-control" name="keyword" value="<?php echo isset($detail_category["keyword"]) ? $detail_category["keyword"] : null;?>"/>
                </div>
                <div class="col-md-12" style="text-align:right;">
                <br/>
                    <button class="btn btn-primary" type="submit"><i class="fa fa-check-circle-o"></i> Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close"> <i class="fa fa-ban"></i>  Cancel</button>
                </div>
            </form>
            <!-- end form for validations -->

            </div>
        </div>
    </div>
</div>
<script>
    $('#keyword').tagsInput();
</script>