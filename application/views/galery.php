<div class="blog-page area-padding">
    <div class="container" style="margin-top:25px;">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- Start Portfolio -page -->
        <div class="awesome-project-1 fix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="awesome-menu ">
              <ul class="project-menu">
                <li>
                  <a href="#" class="active" data-filter="*">All</a>
                </li>
              <?php 
                if(isset($list_cat_galery)){
                  foreach($list_cat_galery as $catgal){
                    echo '
                      <li>
                        <a href="#" data-filter=".img-'.$catgal->id.'">'.$catgal->cat_name.'</a>
                      </li>';
                  }
                }
              ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="awesome-project-content">
          
          <?php
            if(isset($list_galery)){
              foreach($list_galery as $gal){
                echo '
                <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 img-'.$gal->id_cat.'">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="'.base_url().'assets/img/'.$gal->img_loc.'" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="'.base_url().'assets/img/'.$gal->img_loc.'">
                      <h4>'.$gal->alt_text.'</h4>
                      <span>'.$gal->alt_text.'</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
                ';
              }
            }
          ?>

          
        </div>
      </div>
    </div>
  </div>
  <!-- awesome-portfolio end -->
        </div>
      </div>
    </div>
<div>