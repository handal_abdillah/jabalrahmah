<?php
$welcome_text = "";
$prof_img = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
$prof_text = "";
$prof_phone = "";
$prof_web = "";
$prof_mail = "";
$prof_ofchr = "";
$prof_addr = "";
$prof_title = "";
foreach($general_content as $gc){
  if(isset($gc->key_content) && $gc->key_content == "WELCOME_TEXT") $welcome_text = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "IMAGE_PROFILE") $prof_img = base_url()."assets/img/".$gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "DESKRIPSI_PROFILE") $prof_text = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "PHONE_PROFILE") $prof_phone = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "URL_PROFILE") $prof_web = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "EMAIL_PROFILE") $prof_mail = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "OFFICE_HOUR") $prof_ofchr = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "ADDRESS_PROFILE") $prof_addr = $gc->value_content;
  if(isset($gc->key_content) && $gc->key_content == "TITLE_PROFILE") $prof_title = $gc->value_content;
}

$sliders_content = "";
$sliders_img = "";
if(isset($sliders)){
  foreach($sliders as $slider){
    $sliders_img .= '<img src="'.base_url()."assets/img/".$slider->img_slider.'" alt="" title="#slider-direction-'.$slider->id.'" />' ;
    $btn1 = empty($slider->text_btn1) || empty($slider->link_btn1) ? "": '<a class="ready-btn right-btn page-scroll" href="'.$slider->link_btn1.'">'.$slider->text_btn1.'</a>';
    $btn2 = empty($slider->text_btn2) || empty($slider->link_btn2) ? "": '<a class="ready-btn right-btn page-scroll" href="'.$slider->link_btn2.'">'.$slider->text_btn2.'</a>';
    $sliders_content .= '<div id="slider-direction-'.$slider->id.'" class="slider-direction slider-one">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="slider-content">
              <!-- layer 1 -->
              <div class="layer-1-1 hidden-xs wow slideInDown" data-wow-duration="2s" data-wow-delay=".2s">
                <h2 class="title1">'.$slider->title_slider.'</h2>
              </div>
              <!-- layer 2 -->
              <div class="layer-1-2 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                <h1 class="title2">'.$slider->text_slider.'</h1>
              </div>
              <!-- layer 3 -->
              <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                '.$btn1.'
                '.$btn2.'
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
  }
}

?>
  <!-- Start Slider Area -->
  <div id="home" class="slider-area">
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        <!-- <img src="<?php echo base_url();?>assets/img/slider/slider1.jpg" alt="" title="#slider-direction-1" />
        <img src="<?php echo base_url();?>assets/img/slider/slider2.jpg" alt="" title="#slider-direction-2" />
        <img src="<?php echo base_url();?>assets/img/slider/slider3.jpg" alt="" title="#slider-direction-3" /> -->
        <?php echo $sliders_img;?>
      </div>
      <?php 
        
      ?>
      <!-- direction 1 -->
      <?php echo $sliders_content;?>
    </div>
  </div>
  <!-- End Slider Area -->
  <!-- Start Suscrive Area -->
  <div class="suscribe-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs=12">
          <div class="suscribe-text text-center">
            <h3><?php echo $welcome_text; ?></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Suscrive Area -->
  <!-- Start About area -->
  <div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2><?php echo $prof_title;?></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
								  <img src="<?php echo $prof_img;?>" alt="">
								</a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">
              <?php echo $prof_text;?>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End About area -->

  <!-- Start portfolio Area -->
  <div id="portfolio" class="portfolio-area area-padding fix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Galery Photo Pesantren</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start Portfolio -page -->
        <div class="awesome-project-1 fix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="awesome-menu ">
              <ul class="project-menu">
                <li>
                  <a href="#" class="active" data-filter="*">All</a>
                </li>
              <?php 
                if(isset($list_cat_galery)){
                  foreach($list_cat_galery as $catgal){
                    echo '
                      <li>
                        <a href="#" data-filter=".img-'.$catgal->id.'">'.$catgal->cat_name.'</a>
                      </li>';
                  }
                }
              ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="awesome-project-content">
          
          <?php
            if(isset($list_galery)){
              foreach($list_galery as $gal){
                echo '
                <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 img-'.$gal->id_cat.'">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="'.base_url().'assets/img/'.$gal->img_loc.'" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="'.base_url().'assets/img/'.$gal->img_loc.'">
                      <h4>'.$gal->alt_text.'</h4>
                      <span>'.$gal->alt_text.'</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
                ';
              }
            }
          ?>

          
        </div>
      </div>
    </div>
  </div>
  <!-- awesome-portfolio end -->
  <!-- start pricing area -->
  <div id="pricing" class="pricing-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Program Pesantren</h2>
          </div>
        </div>
      </div>
      <div class="row">
      <?php 
        if(isset($list_program)){
          foreach($list_program as $detail_program){
            ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="pri_table_list">
                  <h3><?php echo isset($detail_program->title_program) ? $detail_program->title_program : null;?></h3>
                  <img alt="" src="<?php echo isset($detail_program->image_program) ? base_url()."assets/img/".$detail_program->image_program : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                  <div style="margin-bottom:25px; margin-top:10px;">
                  <?php echo isset($detail_program->desc_program) ? $detail_program->desc_program : null;?>
                  </div>
              </div>
            </div>
            <?php
          }
        }
      ?>
      </div>
    </div>
  </div>
  <!-- End pricing table area -->
  <!-- Start Blog Area -->
  <div id="blog" class="blog-area">
    <div class="blog-inner area-padding">
      <div class="blog-overly"></div>
      <div class="container ">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>Berita</h2>
            </div>
          </div>
        </div>
        <div class="row">
        <?php 
          if(isset($list_berita)){
            foreach($list_berita as $berita){
              if(strlen($berita->posting_headline) > 150){
                $pos=strpos($berita->posting_headline, ' ', 150);
                $headline = substr($berita->posting_headline,0,$pos);
              }else{
                $headline = $berita->posting_headline;
              }
              
              echo '
              <!-- Start Left Blog -->
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="single-blog">
                  <div class="single-blog-img">
                    <a href="blog.html">
                        <img src="'.base_url().'assets/img/'.$berita->image_header.'" alt="">
                      </a>
                  </div>
                  <div class="blog-meta">
                    
                    <span class="date-type">
                        <i class="fa fa-calendar"></i>'.$berita->posting_time.'
                      </span>
                  </div>
                  <div class="blog-text">
                    <h4><a href="'.base_url().'berita/detail/'.$berita->id.'-'.$berita->slug.'">'.$berita->title.'</a></h4>
                    <p>
                    '.$headline.'..
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <a href="'.base_url().'berita/detail/'.$berita->id.'-'.$berita->slug.'" class="btn btn-default btn-block">Read more</a>
                  </div>
                </div>
                <!-- Start single blog -->
              </div>
              <!-- End Left Blog-->
              ';
            }
          }
        ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End Blog -->
  <!-- Start contact Area -->
  <div id="contact" class="contact-area">
    <div class="contact-inner area-padding">
      <div class="contact-overly"></div>
      <div class="container ">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>Contact us</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- Start contact icon column -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-icon text-center">
              <div class="single-icon">
                <i class="fa fa-mobile"></i>
                <p>
                  Call: +<?php echo $prof_ofchr;?><br>
                  <span><?php echo $prof_ofchr;?></span>
                </p>
              </div>
            </div>
          </div>
          <!-- Start contact icon column -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-icon text-center">
              <div class="single-icon">
                <i class="fa fa-envelope-o"></i>
                <p>
                  Email: <?php echo $prof_mail;?><br>
                  <span>Web: <?php echo $prof_web;?></span>
                </p>
              </div>
            </div>
          </div>
          <!-- Start contact icon column -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-icon text-center">
              <div class="single-icon">
                <i class="fa fa-map-marker"></i>
                <p>
                  Location: <?php echo $prof_addr;?><br>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row">

          <!-- Start Google Map -->
          <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- Start Map -->
              <div id="google-map" data-latitude="107.8402884" data-longitude="-6.9730812"></div>
            <!-- End Map -->
          </div>
          <!-- End Google Map -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Contact Area -->

  <!-- Start Footer bottom Area -->
  