<div class="blog-page area-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="page-head-blog">
            <div class="single-blog-page">
              <!-- recent start -->
              <div class="left-blog">
                <h4>recent post</h4>
                <div class="recent-post">
                  
                  <?php 
                    if(isset($recent_articles)){
                      foreach($recent_articles as $rec_art){
                        echo '
                        <!-- start single post -->
                        <div class="recent-single-post">
                          <div class="post-img">
                            <a href="#">
                                  <img src="'.base_url().'assets/img/'.$rec_art->image_header.'" alt="'.$rec_art->title.'">
                              </a>
                          </div>
                          <div class="pst-content">
                            <p><a href="'.base_url().'/berita/detail/'.$rec_art->id.'-'.$rec_art->slug.'">'.$rec_art->title.'</a></p>
                          </div>
                        </div>
                        <!-- End single post -->';
                      }
                    }
                  ?>
                </div>
              </div>
              <!-- recent end -->
            </div>
            <div class="single-blog-page">
              <div class="left-blog">
                <h4>categories</h4>
                <ul>
                <?php 
                  if(isset($list_category)){
                    foreach($list_category as $cat){
                      echo '<li><a href="'.base_url().'berita/category/'.$cat->slug.'">'.$cat->name_category.'</a></li>';
                    }
                  }
                ?>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
        <!-- End left sidebar -->
        <!-- Start single blog -->
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="row">
            <?php 
              if(isset($list_articles)){
                foreach($list_articles as $article){
                  if(strlen($article->posting_headline) > 150){
                    $pos=strpos($article->posting_headline, ' ', 150);
                    $headline = substr($article->posting_headline,0,$pos);
                  }else{
                    $headline = $article->posting_headline;
                  }
                  
                  echo '
                  <!-- End single blog -->
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="single-blog">
                      <div class="single-blog-img">
                        <a href="blog-details.html">
                            <img src="'.base_url().'assets/img/'.$article->image_header.'" alt="">
                          </a>
                      </div>
                      <div class="blog-meta">
                        <span class="comments-type">
                          <a href="'.base_url().'berita/detail/'.$article->id.'-'.$article->slug.'" class="ready-btn">Read more</a>
                        </span>
                        <span class="date-type">
                            <i class="fa fa-calendar"></i>'.$article->posting_time.'
                          </span>
                      </div>
                      <div class="blog-text">
                        <h4>
                            <a href="'.base_url().'berita/detail/'.$article->id.'-'.$article->slug.'">'.$article->title.'</a>
                          </h4>
                        <p>
                        '.$headline.'..
                        </p>
                      </div>
                      
                    </div>
                  </div>
                  <!-- Start single blog -->
                  ';
                }
              }
            
            ?>
            <div class="blog-pagination">
              <ul class="pagination">
              <?php 
                $total_article = $this->data_model->get_total_article();
                for($i=1; $i <= ceil($total_article/NEWS_PERPAGE); $i++){
                  $active_page = $cur_page == $i ? ' class="active"' : null;
                  echo '<li'.$active_page.'><a href="'.base_url().'berita/l/'.$i.'">'.$i.'</a></li>';
                }
              ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>