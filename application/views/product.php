
  <!-- Start Blog Area -->
  <div id="blog" class="blog-area" style="margin-top: 15px;">
    <div class="blog-inner area-padding">
      <div class="blog-overly"></div>
      <div class="container ">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>Product-Product La Tahzan</h2>
            </div>
          </div>
        </div>
        <div class="row">
        <?php 
          if(isset($list_product)){
            foreach($list_product as $prod){
              echo '
          <!-- Start Left Blog -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="single-blog">
              <div class="single-blog-img">
										<img src="'.base_url().'assets/img/'.$prod->image_product.'" alt="">
              </div>
              <div class="blog-text">
                <h4>'.$prod->nama_product.'</h4>
                <p>
                '.$prod->deskripsi_product.'
                </p>
              </div>
              
            </div>
            <!-- Start single blog -->
          </div>
          <!-- End Left Blog-->
              ';
            }
          }
        ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End Blog -->

  <!-- Start Footer bottom Area -->
  