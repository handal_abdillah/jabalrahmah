<?php
$usr_ses = $this->session->userdata();
if(!isset($usr_ses["id"]) || empty($usr_ses["id"])){
    redirect("auth");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Jabalrahmah</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets_admin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets_admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets_admin/nprogress/nprogress.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="<?php echo base_url();?>assets_admin/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>assets_admin/build/css/custom.min.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets_admin/jquery/dist/jquery.min.js"></script>
    <!-- jQuery -->
    
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets_admin/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets_admin/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="<?php echo base_url();?>assets_admin/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets_admin/bootstrap/dist/js/bootstrap.min.js"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url()."admin"?>" class="site_title"><i class="fa fa-gear"></i> <span>Jabalrahmah</span></a>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a href="<? echo base_url();?>admin"><i class="fa fa-home"></i> Home</a> </li>
                  <li><a href="<? echo base_url();?>admin_profile"><i class="fa fa-info"></i> Profile</a> </li>
                  <li><a href="<? echo base_url();?>admin_program"><i class="fa fa-list"></i> Programs</a> </li>
                  <li><a href="<? echo base_url();?>admin_product"><i class="fa fa-table"></i> Product</a> </li>
                  <li><a href="<? echo base_url();?>admin_galery"><i class="fa fa-photo"></i> Galery</a> </li>
                  <li><a><i class="fa fa-edit"></i> News & Article <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<? echo base_url();?>admin_post">News & Article</a></li>
                        <li><a href="<? echo base_url();?>admin_post/category">Category</a></li>
                    </ul>
                  </li>
                  <!-- <li><a><i class="fa fa-desktop"></i> Announce & Donation <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">General Elements</a></li>
                      <li><a href="<? echo base_url();?>admin_galery">Media Gallery</a></li>
                    </ul>
                  </li> -->
                  <li><a><i class="fa fa-gear"></i> Settings <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<? echo base_url();?>admin_user">User Management</a></li>
                      <!-- <li><a href="<? echo base_url();?>admin_seo">SEO</a></li> -->
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url();?>assets_admin/images/<?php echo $usr_ses["image_profile"];?>" alt="">Welcome, <?php echo $usr_ses["full_name"];?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url()?>auth/destroy_sess"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->