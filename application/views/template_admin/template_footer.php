<?php
$notif = $this->session->flashdata("notif");
  if($notif){
    ?>
<link href="<?php echo base_url();?>assets_admin/pnotify/dist/pnotify.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets_admin/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets_admin/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

<!-- PNotify -->
<script src="<?php echo base_url();?>assets_admin/pnotify/dist/pnotify.js"></script>
<script src="<?php echo base_url();?>assets_admin/pnotify/dist/pnotify.buttons.js"></script>
<script src="<?php echo base_url();?>assets_admin/pnotify/dist/pnotify.nonblock.js"></script>

<script>
$(document).ready(function(){
  new PNotify({
      title: '<?php echo isset($notif["type"]) ? $notif["type"] : ""; ?>',
      type: '<?php echo isset($notif["type"]) ? $notif["type"] : ""; ?>',
      text: '<?php echo isset($notif["msg"]) ? $notif["msg"] : ""; ?>',
      nonblock: {
          nonblock: true
      },
      styling: 'bootstrap3',
  });
});
</script>
    
    <?

  }
?>
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets_admin/build/js/custom.min.js"></script>
  </body>
</html>