<!-- start pricing area -->
<div id="pricing" class="pricing-area area-padding"  style="margin-top: 15px;">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Program Pesantren</h2>
          </div>
        </div>
      </div>
      <div class="row">
      <?php 
        if(isset($list_program)){
          foreach($list_program as $detail_program){
            ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="pri_table_list">
                  <h3><?php echo isset($detail_program->title_program) ? $detail_program->title_program : null;?></h3>
                  <img alt="" src="<?php echo isset($detail_program->image_program) ? base_url()."assets/img/".$detail_program->image_program : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>">
                  <div style="margin-bottom:25px; margin-top:10px;">
                  <?php echo isset($detail_program->desc_program) ? $detail_program->desc_program : null;?>
                  </div>
              </div>
            </div>
            <?php
          }
        }
      ?>
      </div>
    </div>
  </div>
  <!-- End pricing table area -->