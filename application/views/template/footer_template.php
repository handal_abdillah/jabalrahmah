
<?php 
$this->load->model("data_model");
$content_footer = $this->data_model->get_general_content();
  

?>
<footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <div class="col-md-12">
                <center><i class="fa fa-map-marker"></i> Alamat: <?php echo $content_footer["prof_addr"];?></center>
                </div>
                <div class="col-md-12">
                <center><span><i class="fa fa-phone"></i> Tel:</span> <?php echo $content_footer["prof_phone"];?></center>
                <center><span><i class="fa fa-envelope-o"></i> Email:</span> <?php echo $content_footer["prof_mail"];?></center>
                <center><span><i class="fa fa-calendar"></i> Working Hours:</span> <?php echo $content_footer["prof_ofchr"];?></center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong>Jabalrahmah</strong>. All Rights Reserved
              </p>
            </div>
            <div class="credits">
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/venobox/venobox.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/knob/jquery.knob.js"></script>
  <script src="<?php echo base_url();?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/parallax/parallax.js"></script>
  <script src="<?php echo base_url();?>assets/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/lib/appear/jquery.appear.js"></script>
  <script src="<?php echo base_url();?>assets/lib/isotope/isotope.pkgd.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvVPhql9dKIJIx7fDRwyM-wibxf0qXjd4"></script>

  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url();?>assets/contactform/contactform.js"></script>

  <script src="<?php echo base_url();?>assets/js/main.js"></script>
</body>

</html>
