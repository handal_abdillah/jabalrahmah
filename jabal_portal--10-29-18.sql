# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.25-MariaDB)
# Database: jabal_portal
# Generation Time: 2018-10-29 09:53:39 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table galery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galery`;

CREATE TABLE `galery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `alt_text` varchar(255) DEFAULT NULL,
  `img_loc` varchar(255) DEFAULT NULL,
  `id_cat` int(11) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `galery` WRITE;
/*!40000 ALTER TABLE `galery` DISABLE KEYS */;

INSERT INTO `galery` (`id`, `alt_text`, `img_loc`, `id_cat`, `created_time`, `updated_time`, `is_active`)
VALUES
	(1,'dsada','Screen_Shot_2018-09-11_at_10_09_50_AM1.png',0,'2018-09-28 21:21:14','2018-10-01 17:48:47',1),
	(2,'alt text ku','Screen_Shot_2018-09-27_at_5_21_27_PM1.png',1,'2018-10-02 16:27:40',NULL,1);

/*!40000 ALTER TABLE `galery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table galery_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galery_category`;

CREATE TABLE `galery_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `galery_category` WRITE;
/*!40000 ALTER TABLE `galery_category` DISABLE KEYS */;

INSERT INTO `galery_category` (`id`, `cat_name`)
VALUES
	(1,'test cate');

/*!40000 ALTER TABLE `galery_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table general_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `general_content`;

CREATE TABLE `general_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key_content` varchar(255) DEFAULT '',
  `value_content` text,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) DEFAULT NULL,
  `initial_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `general_content` WRITE;
/*!40000 ALTER TABLE `general_content` DISABLE KEYS */;

INSERT INTO `general_content` (`id`, `key_content`, `value_content`, `updated_time`, `id_user`, `initial_page`)
VALUES
	(1,'WELCOME_TEXT','test welcome bro','2018-09-19 11:30:23',NULL,'HOME'),
	(32,'IMAGE_PROFILE','Screen_Shot_2018-09-11_at_10_09_21_AM4.png',NULL,NULL,'PROFILE'),
	(61,'TITLE_PROFILE','test title2',NULL,NULL,'PROFILE'),
	(62,'DESKRIPSI_PROFILE','<h4><strong>PROJECT MAINTENANCE</strong></h4>\r\n\r\n<p>Redug Lagre dolor sit amet, consectetur adipisicing elit. Itaque quas officiis iure aspernatur sit adipisci quaerat unde at nequeRedug Lagre dolor sit amet, consectetur adipisicing elit. Itaque quas officiis iure</p>\r\n\r\n<ol>\r\n	<li>&nbsp;Interior design Package</li>\r\n	<li>&nbsp;Building House</li>\r\n	<li>&nbsp;Reparing of Residentail Roof</li>\r\n	<li>&nbsp;Renovaion of Commercial Office</li>\r\n	<li>&nbsp;Make Quality Products</li>\r\n</ol>\r\n',NULL,NULL,'PROFILE'),
	(63,'OFFICE_HOUR','09.00 - 15.00 wib',NULL,NULL,'PROFILE'),
	(64,'ADDRESS_PROFILE','Jl. Titiran, Sadang Serang, Bandung City, West Java, Indonesia',NULL,NULL,'PROFILE'),
	(65,'EMAIL_PROFILE','jabalrahmah@gmail.comf',NULL,NULL,'PROFILE'),
	(66,'PHONE_PROFILE','082237965035',NULL,NULL,'PROFILE'),
	(67,'URL_PROFILE','https://ponpesjabalrahmah.com',NULL,NULL,'PROFILE');

/*!40000 ALTER TABLE `general_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table home_slider
# ------------------------------------------------------------

DROP TABLE IF EXISTS `home_slider`;

CREATE TABLE `home_slider` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text_slider` varchar(250) DEFAULT NULL,
  `text_btn1` varchar(250) DEFAULT NULL,
  `text_btn2` varchar(250) DEFAULT NULL,
  `link_btn1` varchar(250) DEFAULT NULL,
  `link_btn2` varchar(250) DEFAULT NULL,
  `aktif_slider` tinyint(1) DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) DEFAULT NULL,
  `img_slider` varchar(255) DEFAULT NULL,
  `title_slider` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `home_slider` WRITE;
/*!40000 ALTER TABLE `home_slider` DISABLE KEYS */;

INSERT INTO `home_slider` (`id`, `text_slider`, `text_btn1`, `text_btn2`, `link_btn1`, `link_btn2`, `aktif_slider`, `updated_time`, `id_user`, `img_slider`, `title_slider`)
VALUES
	(1,'text slider ku','czxzczczx','','link ku 1','link 2',1,'2018-10-14 11:12:07',NULL,'Screen_Shot_2018-09-17_at_9_55_35_AM.png','test gann'),
	(2,'test lagi ah','hsajkdhjak','btn 2','chasdjkhakjhda','hcadskjdhsakj',0,'2018-09-18 16:29:00',NULL,'Screen_Shot_2018-09-05_at_10_15_49_PM.png',NULL);

/*!40000 ALTER TABLE `home_slider` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table post_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `post_category`;

CREATE TABLE `post_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name_category` varchar(255) DEFAULT NULL,
  `desc_category` varchar(255) DEFAULT NULL,
  `slug` varchar(20) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `post_category` WRITE;
/*!40000 ALTER TABLE `post_category` DISABLE KEYS */;

INSERT INTO `post_category` (`id`, `name_category`, `desc_category`, `slug`, `keyword`, `updated_time`)
VALUES
	(1,'kategori 1','deksripsi kategori 1','slug1','keyword 1','2018-09-19 10:59:41'),
	(2,'kategori 2','deskripsi kategori 2','slug 1','keyword1','2018-09-19 11:32:31'),
	(3,'kategori 3','desk kategory 3','slug 1 2 3','test,tes2,kayword2','2018-09-19 11:49:23');

/*!40000 ALTER TABLE `post_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posting`;

CREATE TABLE `posting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `posting_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `image_header` varchar(255) DEFAULT NULL,
  `posting_content` mediumtext,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `posting` WRITE;
/*!40000 ALTER TABLE `posting` DISABLE KEYS */;

INSERT INTO `posting` (`id`, `id_category`, `title`, `author`, `tags`, `posting_time`, `image_header`, `posting_content`, `slug`)
VALUES
	(1,NULL,'dsajkdhakjhdkahdka',NULL,'testete,dhasjdhja,ddajsdnsja','2018-09-25 17:42:41',NULL,'<p><img alt=\"\" src=\"http://localhost/jabalrahmah/images_article/images/Screen%20Shot%202018-09-11%20at%2010.09.08%20AM.png\" style=\"height:180px; width:288px\" /></p>\r\n\r\n<p>aku cinta kepadamu aku rindu di pelukmu</p>\r\n\r\n<p>&nbsp;</p>\r\n','djsajhjkajd'),
	(2,NULL,'dsajkdhakjhdkahdka',NULL,'djaskjak,dhasjdhaj','2018-09-25 17:42:57',NULL,'<p><img alt=\"\" src=\"http://localhost/jabalrahmah/images_article/images/Screen%20Shot%202018-09-11%20at%2010.09.08%20AM.png\" style=\"height:180px; width:288px\" /></p>\r\n\r\n<p>aku cinta kepadamu aku rindu di pelukmu</p>\r\n\r\n<p>&nbsp;</p>\r\n','dsajkdhakjhdkahdka'),
	(3,2,'adhasjkhak',NULL,'hdhjsahdaj','2018-09-25 18:23:26','Screen_Shot_2018-09-11_at_10_09_01_AM2.png','<p>djshajahjahjsaj</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/jabalrahmah/images_article/images/Screen%20Shot%202018-09-11%20at%2010.09.08%20AM.png\" style=\"height:1800px; width:2880px\" />shdjashjdahjsahj</p>\r\n','adhasjkhak'),
	(4,NULL,'dhasjkhak',NULL,'dnjsabja,dhkjaskda,xbajbja','2018-09-25 18:24:05','Screen_Shot_2018-09-04_at_2_39_32_PM1.png','<p>djshajahjahjsaj</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/jabalrahmah/images_article/images/Screen%20Shot%202018-09-11%20at%2010.09.08%20AM.png\" style=\"height:1800px; width:2880px\" />shdjashjdahjsahj</p>\r\n','dhsajkhakjk923719'),
	(5,NULL,'hdjashdjahja',NULL,'sdhkashkja,nbchasvhdjahj,xbnas bnxa','2018-09-25 18:24:25','Screen_Shot_2018-09-11_at_10_08_49_AM2.png','<p>nxjksgdjkdakjh</p>\r\n\r\n<p><img alt=\"\" src=\"http://localhost/jabalrahmah/images_article/images/Screen%20Shot%202018-09-05%20at%204.06.43%20PM.png\" style=\"height:475px; width:500px\" /></p>\r\n\r\n<p>dhjakshdjkahjk</p>\r\n','ddsahjahja');

/*!40000 ALTER TABLE `posting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_product` varchar(255) DEFAULT NULL,
  `image_product` varchar(255) DEFAULT NULL,
  `deskripsi_product` text,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;

INSERT INTO `product` (`id`, `nama_product`, `image_product`, `deskripsi_product`, `created_time`, `updated_time`)
VALUES
	(1,'product 2','Screen_Shot_2018-09-11_at_10_08_49_AM3.png','dhsjkadakhkjahdjka1111','2018-10-02 18:46:25','2018-10-02 18:52:25');

/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table program
# ------------------------------------------------------------

DROP TABLE IF EXISTS `program`;

CREATE TABLE `program` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title_program` varchar(255) DEFAULT NULL,
  `desc_program` text,
  `image_program` varchar(255) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) DEFAULT NULL,
  `aktif` tinyint(1) DEFAULT NULL,
  `type_program` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;

INSERT INTO `program` (`id`, `title_program`, `desc_program`, `image_program`, `updated_time`, `id_user`, `aktif`, `type_program`)
VALUES
	(1,'Program1','Deskripsi Program 1','Screen_Shot_2018-10-12_at_1_07_08_PM.png',NULL,NULL,1,NULL);

/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_member`;

CREATE TABLE `user_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `is_aktif` tinyint(1) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `image_profile` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_member` WRITE;
/*!40000 ALTER TABLE `user_member` DISABLE KEYS */;

INSERT INTO `user_member` (`id`, `email`, `full_name`, `pass`, `user_type`, `is_aktif`, `created_time`, `image_profile`, `last_login`)
VALUES
	(1,'hsja@dhja','handal','$2y$10$XoMbIE.tl/NBPruYQodn4eEkb7ANV4Cp/XM2jlq/2pBq95dCCUsXG',NULL,1,'2018-09-26 15:30:59','Screen_Shot_2018-09-17_at_9_55_35_AM1.png',NULL),
	(2,'a@b.c','handal','$2y$10$H3Iy6iPkndpdQmR5p3BC0OXRgC/hkST592iS4RGTl3LnuZ1HZHYU6',NULL,1,'2018-09-28 13:41:38','Screen_Shot_2018-09-27_at_5_21_27_PM.png','2018-10-29 10:00:50');

/*!40000 ALTER TABLE `user_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `usert_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
